package com.gt.app.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.model.Company;
import com.gt.pageModel.Json;
import com.gt.sys.controller.BaseController;
import com.gt.app.service.ICompanyService;


/**
 * @功能说明：公司管理
 * @作者：Iron
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
@Controller
@RequestMapping("/company")

public class CompanyController extends BaseController {
	private Logger logger = Logger.getLogger(CompanyController.class);
	private ICompanyService companyService;

	public ICompanyService getCompanyService() {
		return  companyService;
	}

	@Autowired
	public void setCompanyService(ICompanyService companyService) {
		this.companyService = companyService;
	}


	/**
	 * 获取datagrid数据
	 */
	@RequestMapping("/datagrid")
	@ResponseBody
	public DatagridForLayUI datagrid(Company company) {
		DatagridForLayUI datagrid=null;
		try {
			datagrid = companyService.datagrid(company);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return datagrid;
	}

	/**
	 * 新增
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Json add(Company company, HttpServletRequest request, HttpSession session) {
		Json j = new Json();
		try {
			j = companyService.add(company);
		} catch (Exception e) {
			logger.error(e.getMessage());
			j.setSuccess(false);
			e.printStackTrace();
			j.setMsg("添加失败:" + e.getMessage());
		}
		
		//添加系统日志
		writeSysLog(j, company, request, session);
		
		return j;
	}

	/**
	 * 删除
	 */
	@RequestMapping("/remove")
	@ResponseBody
	public Json remove(Company company, HttpServletRequest request, HttpSession session) {
		Json j = new Json();
		try {
			companyService.remove(company.getId().toString());
			j.setSuccess(true);
			j.setMsg("删除成功！");
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("删除失败:" + e.getMessage());
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		//添加系统日志
		writeSysLog(j, company,request, session);
		
		return j;
	}

	/**
	 * 修改
	 */
	@RequestMapping("/modify")
	@ResponseBody
	public Json modify(Company company, HttpServletRequest request, HttpSession session) {
		Json j = new Json();
		try {
			j = companyService.modify(company);
		} catch (Exception e) {
			logger.error(e.getMessage());
			j.setSuccess(false);
			j.setMsg("修改失败:" + e.getMessage());
			e.printStackTrace();
		}
	
		//添加系统日志
		writeSysLog(j, company,request,session );
		return j;
	}

	/**
	 * 获取下拉框数据
	 */
	@RequestMapping("/getList")
	@ResponseBody
	public List<Company>  getList() {
		List<Company> list = companyService.getList();
		return list;
	}

}
