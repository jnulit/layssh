package com.gt.app.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.gt.utils.MyBeanUtils;
import org.springframework.stereotype.Service;
import com.gt.app.service.ICompanyService;
import com.gt.model.Company;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.pageModel.Json;
import com.gt.sys.service.impl.BaseServiceImpl;
import com.gt.utils.PbUtils;


/**
 * 
 * @功能说明：公司管理业务接口实现类
 * @作者： Iron
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
@Service("companyService")
public class CompanyServiceImpl extends BaseServiceImpl<Company> implements ICompanyService {

	@Override
	public DatagridForLayUI datagrid(Company company) throws Exception{
		DatagridForLayUI grid = new DatagridForLayUI();
		String sqlLeft = "select t.id as id,t.address as address,t.clicks as clicks,t.description as description,t.establishment_date as establishmentDate,t.name as name,t.register_date as registerDate,t.registered_capital as registeredCapital,t.representative as representative,t.reviews as reviews,t.telephone as telephone " ;
		String sql = " from company t where 1=1 " ;
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		// 
		if (!PbUtils.isEmpty(company.getId())) {
			sql += " and t.id like:id";
			param.put("id", "%%" + company.getId() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getAddress())) {
			sql += " and t.address like:address";
			param.put("address", "%%" + company.getAddress() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getClicks())) {
			sql += " and t.clicks like:clicks";
			param.put("clicks", "%%" + company.getClicks() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getDescription())) {
			sql += " and t.description like:description";
			param.put("description", "%%" + company.getDescription() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getEstablishmentDate())) {
			sql += " and t.establishment_date like:establishmentDate";
			param.put("establishmentDate", "%%" + company.getEstablishmentDate() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getName())) {
			sql += " and t.name like:name";
			param.put("name", "%%" + company.getName() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getRegisterDate())) {
			sql += " and t.register_date like:registerDate";
			param.put("registerDate", "%%" + company.getRegisterDate() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getRegisteredCapital())) {
			sql += " and t.registered_capital like:registeredCapital";
			param.put("registeredCapital", "%%" + company.getRegisteredCapital() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getRepresentative())) {
			sql += " and t.representative like:representative";
			param.put("representative", "%%" + company.getRepresentative() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getReviews())) {
			sql += " and t.reviews like:reviews";
			param.put("reviews", "%%" + company.getReviews() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(company.getTelephone())) {
			sql += " and t.telephone like:telephone";
			param.put("telephone", "%%" + company.getTelephone() + "%%");
		}
		
		
		String totalsql = "select count(*) " + sql;

		//排序
		sql += " order by  id desc";
	
		List<Map> maps = findBySql(sqlLeft+sql, param, company.getPage(), company.getLimit());
		Long total = countBySql(totalsql,param);
		grid.setCount(total);
		grid.setData(maps);

		return grid;
	}
	
	
	@Override
	public Json add(Company inf) {
		Json j = new Json();
		save(inf);
		j.setSuccess(true);
		j.setMsg("新增成功");
		j.setObj(inf);	// 设置返回对象
		return j;

	}

	@Override
	public void remove(String ids) {
		String[] nids = ids.split(",");
		String sql = "delete from company  where id in (";
		for (int i = 0; i < nids.length; i++) {
			if (i > 0) {
				sql += ",";
			}
			sql += "'" + nids[i] + "'";
		}
		sql += ")";
		executeSql(sql);

	}

	@Override
	public Json modify(Company inf) {
		Json j = new Json();

		Company tInf = getById(Company.class, inf.getId());
		if (tInf == null) {
			j.setSuccess(false);
			j.setMsg("修改失败：找不到要修改的信息");
			return j;
		}

		// 旧对象
		Company oldObject = new Company();
		MyBeanUtils.copyProperties(tInf, oldObject);
		j.setOldObj(oldObject);
		
		MyBeanUtils.copyProperties(inf, tInf);
		update(tInf);// 更新
		j.setSuccess(true);
		j.setMsg("更新成功");
		j.setObj(tInf);// 设置返回对象
		return j;
	}

	
	@Override
	public Json verifyInfo(Company inf) {
		Json j = new Json();
		Map<String, Object> params= new HashMap<String, Object>();
		if (!PbUtils.isEmpty(inf.getId())) {
			params.put("id", inf.getId());
		}
		Long total = super.count("Select count(*) from Company t where t.Id =:id ", params);

		if (total > 0) {
			j.setSuccess(false);
			j.setMsg("此信息已经存在");
			return j;
		}

		j.setSuccess(true);
		j.setMsg("成功！");
		j.setObj(inf);
		return j;
	}

	@Override
	public List<Company> getList() {
		List<Company> l = find("from Company t");
		return l;
	}
}
