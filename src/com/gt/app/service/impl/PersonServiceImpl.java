package com.gt.app.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.gt.utils.MyBeanUtils;
import org.springframework.stereotype.Service;
import com.gt.app.service.IPersonService;
import com.gt.model.Person;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.pageModel.Json;
import com.gt.sys.service.impl.BaseServiceImpl;
import com.gt.utils.PbUtils;


/**
 * 
 * @功能说明：人事管理业务接口实现类
 * @作者： herun
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
@Service("personService")
public class PersonServiceImpl extends BaseServiceImpl<Person> implements IPersonService {

	@Override
	public DatagridForLayUI datagrid(Person person) throws Exception{
		DatagridForLayUI grid = new DatagridForLayUI();
		String sqlLeft = "select t.id as id,t.firstName as firstname,t.lastName as lastname,t.region as region,t.startDate as startdate,t.version as version " ;
		String sql = " from person t where 1=1 " ;
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		// 
		if (!PbUtils.isEmpty(person.getId())) {
			sql += " and t.id like:id";
			param.put("id", "%%" + person.getId() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(person.getFirstname())) {
			sql += " and t.firstName like:firstname";
			param.put("firstname", "%%" + person.getFirstname() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(person.getLastname())) {
			sql += " and t.lastName like:lastname";
			param.put("lastname", "%%" + person.getLastname() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(person.getRegion())) {
			sql += " and t.region like:region";
			param.put("region", "%%" + person.getRegion() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(person.getStartdate())) {
			sql += " and t.startDate like:startdate";
			param.put("startdate", "%%" + person.getStartdate() + "%%");
		}
		// 
		if (!PbUtils.isEmpty(person.getVersion())) {
			sql += " and t.version like:version";
			param.put("version", "%%" + person.getVersion() + "%%");
		}
		
		
		String totalsql = "select count(*) " + sql;

		//排序
		sql += " order by  id desc";
	
		List<Map> maps = findBySql(sqlLeft+sql, param, person.getPage(), person.getLimit());
		Long total = countBySql(totalsql,param);
		grid.setCount(total);
		grid.setData(maps);

		return grid;
	}
	
	
	@Override
	public Json add(Person inf) {
		Json j = new Json();
		save(inf);
		j.setSuccess(true);
		j.setMsg("新增成功");
		j.setObj(inf);	// 设置返回对象
		return j;

	}

	@Override
	public void remove(String ids) {
		String[] nids = ids.split(",");
		String sql = "delete from person  where id in (";
		for (int i = 0; i < nids.length; i++) {
			if (i > 0) {
				sql += ",";
			}
			sql += "'" + nids[i] + "'";
		}
		sql += ")";
		executeSql(sql);

	}

	@Override
	public Json modify(Person inf) {
		Json j = new Json();

		Person tInf = getById(Person.class, inf.getId());
		if (tInf == null) {
			j.setSuccess(false);
			j.setMsg("修改失败：找不到要修改的信息");
			return j;
		}

		// 旧对象
		Person oldObject = new Person();
		MyBeanUtils.copyProperties(tInf, oldObject);
		j.setOldObj(oldObject);
		
		MyBeanUtils.copyProperties(inf, tInf);
		update(tInf);// 更新
		j.setSuccess(true);
		j.setMsg("更新成功");
		j.setObj(tInf);// 设置返回对象
		return j;
	}

	
	@Override
	public Json verifyInfo(Person inf) {
		Json j = new Json();
		Map<String, Object> params= new HashMap<String, Object>();
		if (!PbUtils.isEmpty(inf.getId())) {
			params.put("id", inf.getId());
		}
		Long total = super.count("Select count(*) from Person t where t.Id =:id ", params);

		if (total > 0) {
			j.setSuccess(false);
			j.setMsg("此信息已经存在");
			return j;
		}

		j.setSuccess(true);
		j.setMsg("成功！");
		j.setObj(inf);
		return j;
	}

	@Override
	public List<Person> getList() {
		List<Person> l = find("from Person t");
		return l;
	}
}
