package com.gt.app.service;

import java.util.List;
import com.gt.model.Person;
import com.gt.pageModel.DatagridForLayUI;
import com.gt.pageModel.Json;
import com.gt.sys.service.IBaseService;

/**
 * @功能说明：人事管理业务接口类
 * @作者： herun
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
public interface IPersonService extends IBaseService<Person> {

	/**
	 * 获取datagrid数据
	 * 
	 * @return
	 */
	public DatagridForLayUI datagrid(Person inf) throws Exception;

	/**
	 * 新增
	 * 
	 * @param inf
	 * @return
	 */
	public Json add(Person inf) throws Exception;

	/**
	 * 删除
	 * 
	 * @param ids
	 */
	public void remove(String ids);

	/**
	 * 修改
	 * 
	 * @param menuInf
	 */
	public Json modify(Person inf);

	/**
	 * 信息验证
	 * 
	 * @param inf
	 * @return
	 */
	public Json verifyInfo(Person inf);

	/**
	 * 获取下拉框数据
	 * @return
	 */
	public List<Person> getList();

	
}
