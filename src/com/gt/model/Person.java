package com.gt.model;

import com.gt.pageModel.BasePageForLayUI;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @功能说明：人事管理
 * @作者： herun
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
@Entity
@Table(name = "person", schema = "")
public class Person extends BasePageForLayUI implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;//

	private String firstname;//

	private String lastname;//

	private String region;//

	private Date startdate;//

	private Integer version;//


	//构造方法
	public Person() {
	}
	
	
	@Id @Column( name = "id"  ,nullable = false  , length = 10  )
	public Integer getId() {
		return  id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@Column( name = "firstName"  , length = 255  )
	public String getFirstname() {
		return  firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	
	@Column( name = "lastName"  , length = 255  )
	public String getLastname() {
		return  lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	
	@Column( name = "region"  , length = 255  )
	public String getRegion() {
		return  region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	
	 @Temporal(TemporalType.TIMESTAMP) @Column( name = "startDate"  , length = 10  )
	public Date getStartdate() {
		return  startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	
	@Column( name = "version"  , length = 10  )
	public Integer getVersion() {
		return  version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

}
