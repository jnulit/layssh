package com.gt.model;

import com.gt.pageModel.BasePageForLayUI;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @功能说明：公司管理
 * @作者： Iron
 * @创建日期：2018-03-09
 * @版本号：V1.0
 */
@Entity
@Table(name = "company", schema = "")
public class Company extends BasePageForLayUI implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;//

	private String address;//

	private Integer clicks;//

	private String description;//

	private Date establishmentDate;//

	private String name;//

	private Date registerDate;//

	private Long registeredCapital;//

	private String representative;//

	private Integer reviews;//

	private String telephone;//


	//构造方法
	public Company() {
	}
	
	
	@Id @Column( name = "id"  ,nullable = false  , length = 10  )
	public Integer getId() {
		return  id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@Column( name = "address"  , length = 255  )
	public String getAddress() {
		return  address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	@Column( name = "clicks"  ,nullable = false  , length = 10  )
	public Integer getClicks() {
		return  clicks;
	}

	public void setClicks(Integer clicks) {
		this.clicks = clicks;
	}

	
	@Column( name = "description"  , length = 2048  )
	public String getDescription() {
		return  description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	 @Temporal(TemporalType.TIMESTAMP) @Column( name = "establishment_date"  , length = 10  )
	public Date getEstablishmentDate() {
		return  establishmentDate;
	}

	public void setEstablishmentDate(Date establishmentDate) {
		this.establishmentDate = establishmentDate;
	}

	
	@Column( name = "name"  , length = 255  )
	public String getName() {
		return  name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	 @Temporal(TemporalType.TIMESTAMP) @Column( name = "register_date"  , length = 10  )
	public Date getRegisterDate() {
		return  registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	
	@Column( name = "registered_capital"  ,nullable = false  , length = 10  )
	public Long getRegisteredCapital() {
		return  registeredCapital;
	}

	public void setRegisteredCapital(Long registeredCapital) {
		this.registeredCapital = registeredCapital;
	}

	
	@Column( name = "representative"  , length = 255  )
	public String getRepresentative() {
		return  representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	
	@Column( name = "reviews"  ,nullable = false  , length = 10  )
	public Integer getReviews() {
		return  reviews;
	}

	public void setReviews(Integer reviews) {
		this.reviews = reviews;
	}

	
	@Column( name = "telephone"  , length = 255  )
	public String getTelephone() {
		return  telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

}
